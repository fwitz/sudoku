#ifndef GAMERENDERINGFUNCTIONS_H_INCLUDED
#define GAMERENDERINGFUNCTIONS_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

int createMenu(SDL_Surface* ,SDL_Renderer* ,TTF_Font* ,int ,char**);
int drawGrid(SDL_Renderer*);
int drawBG(SDL_Renderer*);
void drawNumberButton(SDL_Renderer*,TTF_Font*);
void writeNumber(SDL_Renderer*, TTF_Font*, int, int, int, int);
void writeTimer(SDL_Renderer*,TTF_Font*, int, int, int);

#endif // GAMERENDERINGFUNCTIONS_H_INCLUDED
