#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include <stdlib.h>

#define SQUARE_SIZE 78

//Function that create and display the main menu
int createMenu(SDL_Surface* screen, SDL_Renderer* renderer, TTF_Font* font, int numberOfOptions, char* labels[numberOfOptions]){

    int x,y;
    SDL_Surface* menus[numberOfOptions];
    SDL_Texture* menusT[numberOfOptions];
    int selected = 0;
    SDL_Color color[2] = {{255,255,255},{173,143,143}};
    SDL_Rect pos[numberOfOptions];

    //Loop that create the texture from the labels passed in args and place them onto the screen
    for(int i=0;i<numberOfOptions;i++){
        TTF_SizeText(font, labels[i],NULL,NULL);
        if(i == 0){
            menus[i]=TTF_RenderText_Blended(font,labels[i],color[1]);
            menusT[i]=SDL_CreateTextureFromSurface(renderer, menus[i]);
            pos[0].y=screen->clip_rect.h/2 - menus[0]->clip_rect.h/2+55;
        }else{
            menus[i]=TTF_RenderText_Blended(font,labels[i],color[0]);
            menusT[i]=SDL_CreateTextureFromSurface(renderer, menus[i]);
            pos[i].y = screen->clip_rect.h/2 + 2*i*menus[i-1]->clip_rect.h/2 - menus[i]->clip_rect.h/2+55;
        }
        pos[i].x = screen->clip_rect.w/2 - menus[i]->clip_rect.w/2+14;
        pos[i].h = menus[i]->clip_rect.h*0.9;
        pos[i].w=menus[i]->clip_rect.w*0.9;
        SDL_RenderCopy(renderer,menusT[i],NULL,&pos[i]);
    }
    SDL_RenderPresent(renderer);

    //Menu loop
    SDL_Event event;
    int continueLoop=1;
    while(continueLoop){
        while(SDL_PollEvent(&event)){
            switch(event.type){
                //If the player quit, we free all the object's we've created and we exit the loop
                case SDL_QUIT:
                    continueLoop=0;
                //Getting the mouse motion on the screen to update the menu
                case SDL_MOUSEMOTION:
                    x=event.motion.x;
                    y=event.motion.y;
                    //Each time the mouse move, we check if the cursor is placed on one of the menu's options
                    for(int i=0;i<numberOfOptions;i++){
                        if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h){
                            if(selected != i){
                                SDL_FreeSurface(menus[selected]);
                                SDL_FreeSurface(menus[i]);
                                menus[i]=TTF_RenderText_Blended(font,labels[i], color[1]);
                                menus[selected]=TTF_RenderText_Blended(font,labels[selected], color[0]);
                                menusT[i]=SDL_CreateTextureFromSurface(renderer, menus[i]);
                                menusT[selected]=SDL_CreateTextureFromSurface(renderer, menus[selected]);
                                SDL_RenderCopy(renderer,menusT[i],NULL,&pos[i]);
                                SDL_RenderCopy(renderer,menusT[selected],NULL,&pos[selected]);
                                SDL_RenderPresent(renderer);
                                selected = i;
                            }
                        }
                    }
                    break;
                //Getting the mouse click to launch the proper action
                case SDL_MOUSEBUTTONDOWN:
                    x=event.motion.x;
                    y=event.motion.y;
                    if(event.button.button ==  SDL_BUTTON_LEFT){
                        //First we check on what option the player click, and then we launch the proper action
                        for(int i=0;i<numberOfOptions;i++){
                            if(x>=pos[i].x && x<=pos[i].x+pos[i].w && y>=pos[i].y && y<=pos[i].y+pos[i].h && event.button.clicks != 0){
                                if(strcmp(labels[i],"Quitter")==0){
                                    for(int j=0;j<numberOfOptions;j++){
                                        SDL_FreeSurface(menus[j]);
                                        continueLoop=0;
                                        return 0;
                                    }
                                }else if(strcmp(labels[i], "Jouer")==0){
                                    return 1;
                                }
                            }
                        }
                    }
                    break;
            }
        }

    }
    //Freeing memory
    for(int i=0;i<numberOfOptions;i++){
        SDL_FreeSurface(menus[i]);
        SDL_DestroyTexture(menusT[i]);
    }
}

//Function that draw the background of the window
int drawBG(SDL_Renderer* renderer){
    //Changing background
    SDL_Surface* bg=NULL;
    SDL_Texture* bgT=NULL;
    bg=SDL_LoadBMP("images/bgGame.bmp");
    //We check if the image load properly
    if(bg == NULL){
        printf("Couldn't load the image: %s\n", SDL_GetError());
        return -1;
    }
    bgT=SDL_CreateTextureFromSurface(renderer,bg);
    SDL_RenderCopy(renderer, bgT, NULL, NULL);
    SDL_FreeSurface(bg);
    SDL_DestroyTexture(bgT);
    return 0;
}

//Function that load the background and draw the game gui
int drawGrid(SDL_Renderer* renderer){

    SDL_SetRenderDrawColor(renderer,255,255,255,SDL_ALPHA_OPAQUE);//Set the color of the lines
    //Draw the grid
    SDL_Rect horizontal;
    SDL_Rect vertical;
    horizontal.w=703;
    horizontal.x=100;
    vertical.h=703;
    vertical.y=100;

    for(int i=100;i<=802;i=i+SQUARE_SIZE){

        //If the lines define the outline of a 3x3 square, we make it more thick
        if((i==334)||(i==568)){
            vertical.w=6;
            horizontal.h=6;
        }else{
            vertical.w=2;
            horizontal.h=2;
        }

        horizontal.y=i;
        vertical.x=i;
        SDL_RenderDrawRect(renderer,&horizontal);
        SDL_RenderFillRect(renderer,&horizontal);
        SDL_RenderDrawRect(renderer,&vertical);
        SDL_RenderFillRect(renderer,&vertical);
    }

    vertical.y=818;
    vertical.w=2;
    vertical.h=SQUARE_SIZE;
    horizontal.h=2;

    horizontal.y=818;
    SDL_RenderDrawRect(renderer, &horizontal);
    horizontal.y=818+SQUARE_SIZE;
    SDL_RenderDrawRect(renderer, &horizontal);

    for(int i=100;i<=802;i=i+SQUARE_SIZE){
        vertical.x=i;
        SDL_RenderDrawRect(renderer, &vertical);
    }



    return 0;//If all has been done correctly
}

//Function that print a number on the grid
void writeNumber(SDL_Renderer* renderer, TTF_Font* font,int value, int posX, int posY, int colorIndex){

    SDL_Surface* number;
    SDL_Texture* numberT;
    SDL_Rect posNumber;
    SDL_Color color[2] = {{255,255,255},{127, 132, 140}};
    char snum[2];
    sprintf(snum, "%d", value);//Transform the value in string

    //Printing the value
    number = TTF_RenderText_Blended(font, snum, color[colorIndex]);
    numberT = SDL_CreateTextureFromSurface(renderer, number);
    posNumber.h = 60;
    posNumber.w = 55;
    posNumber.y = posX*SQUARE_SIZE + 115;
    posNumber.x = posY*SQUARE_SIZE + 112;
    SDL_RenderCopy(renderer,numberT,NULL,&posNumber);
    SDL_FreeSurface(number);
    SDL_DestroyTexture(numberT);

}

void drawNumberButton(SDL_Renderer* renderer, TTF_Font* font){

    SDL_Surface* number;
    SDL_Texture* numberT;
    SDL_Rect posNumber;
    SDL_Color color = {255,255,255};
    char snum[2];

    posNumber.y=818+SQUARE_SIZE/4;
    posNumber.h = 60;
    posNumber.w = 55;

    for(int i=1;i<=9;i++){
        sprintf(snum, "%d", i);
        number = TTF_RenderText_Blended(font, snum, color);
        numberT = SDL_CreateTextureFromSurface(renderer, number);
        posNumber.x=i*SQUARE_SIZE+SQUARE_SIZE/2;
        SDL_RenderCopy(renderer,numberT,NULL,&posNumber);
        SDL_FreeSurface(number);
        SDL_DestroyTexture(numberT);
    }

}

void writeTimer(SDL_Renderer* renderer,TTF_Font* font ,int seconds, int minute, int hours){

    SDL_Surface* timer;
    SDL_Texture* timerT;
    SDL_Rect posTimer;
    SDL_Color color = {255,255,255};
    char* secondsS=malloc(3*sizeof(secondsS));
    char* minuteS=malloc(3*sizeof(minuteS));
    char* hoursS=malloc(3*sizeof(hoursS));
    char* time =malloc(11*sizeof(time));

    posTimer.y=25;
    posTimer.w = 200;
    posTimer.x=350;
    posTimer.h = 60;

    if(seconds < 10){
       sprintf(secondsS, "0%d", seconds);
    }else{
        sprintf(secondsS, "%d", seconds);
    }

    if(minute < 10){
        sprintf(minuteS, "0%d", minute);
    }else{
        sprintf(minuteS, "%d", minute);
    }

    sprintf(hoursS, "0%d", hours);
    snprintf(time,10, "%s:%s:%s", hoursS, minuteS,secondsS);

    timer = TTF_RenderText_Blended(font, time, color);
    timerT = SDL_CreateTextureFromSurface(renderer, timer);
    SDL_RenderCopy(renderer,timerT,NULL,&posTimer);
    SDL_FreeSurface(timer);
    SDL_DestroyTexture(timerT);
    free(secondsS);
    free(minuteS);
    free(hoursS);
    free(time);

}
