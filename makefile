CFLAGS= -W -Wall -ansi -pedantic -Q -std=c99 
LFLAGS= -lSDL2main -lSDL2 -lSDL2_ttf 
EXEC=sudoku
SRC=main.c gameFunctions.c gameRenderingFunctions.c
OBJ=$(SRC:.c=.o)

all: $(EXEC)

sudoku: main.o gameFunctions.o gameRenderingFunctions.o
	gcc -o $@ $^ $(CFLAGS) $(LFLAGS)

main.o: gameFunctions.h gameRenderingFunctions.h

gameFunctions.o: gameRenderingFunctions.h

%.o: %.c
	gcc -o $@ -c $< $(CFLAGS)
clean: 
	rm -rf *.o

mrproper: clean
	rm -rf sudoku
