#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "gameRenderingFunctions.h"
#include "gameFunctions.h"

#define SQUARE_SIZE 78
#define OFFSET 102


int main(){
    //Init SDL and check if it launchs correctly
    if(SDL_Init(SDL_INIT_VIDEO) == 0){
        //Creating the window's, renderer's, surface's and font's pointers
        SDL_Window* sudokuGame = NULL;
        SDL_Renderer* sudokuRender = NULL;
        SDL_Surface* sGSurface = NULL;
        TTF_Font* font = NULL;
        char* labels[3] = {"Jouer", "Quitter"}; //Array of string for the menu

        //Creating dynamically an three dimension array for the grid
        int** grid;
        int** gridPermission;
        int** gridSave;

        grid=malloc(9*sizeof(*grid));
        for(int i=0;i<9;i++){
            grid[i]=malloc(9*sizeof(**grid));
        }

        gridPermission=malloc(9*sizeof(*gridPermission));
        for(int i=0;i<9;i++){
            gridPermission[i]=malloc(9*sizeof(**gridPermission));
        }

        gridSave = malloc(9*sizeof(*gridSave));
        for(int i=0;i<9;i++){
            gridSave[i]=malloc(9*sizeof(**gridSave));
        }

        //Init TTF and loading a font
        TTF_Init();
        font = TTF_OpenFont("fonts/Sudoku.ttf", 100);
        //Checking if the font loaded correctly
        if(font == NULL){
            SDL_Log("Couldn't load the font: %s\n", TTF_GetError());
            return -1;
        }

        sudokuGame = SDL_CreateWindow("Sudoku",300,100,900,900,SDL_SWSURFACE);//Creating the window

        //Continue if the windows launched correctly
        if(sudokuGame){
            sudokuRender = SDL_CreateRenderer(sudokuGame,-1,SDL_RENDERER_ACCELERATED);//Creating the renderer
            //Continue if the renderer has been created correctly
            if(sudokuRender){
                 sGSurface = SDL_GetWindowSurface(sudokuGame); //Getting the surface of the window

                //Setting background
                SDL_Surface* bg = NULL;
                SDL_Texture* bgT = NULL;
                bg=SDL_LoadBMP("images/bg.bmp");
                //Checking if the image load correctly
                if(bg == NULL){
                    SDL_Log("Couldn't load the image: %s\n", SDL_GetError());
                    return -1;
                }
                bgT = SDL_CreateTextureFromSurface(sudokuRender,bg);
                SDL_RenderCopy(sudokuRender, bgT, NULL, NULL);
                SDL_FreeSurface(bg);
                SDL_DestroyTexture(bgT);

                int gameState=0;
                gameState = createMenu(sGSurface,sudokuRender,font, 2, labels);//Function that show and handle the main menu

                //If the playe choose to start the game, we launch the game functions
                if(gameState == 1){

                    int gameWinDrawed;
                    drawBG(sudokuRender);
                    gameWinDrawed=drawGrid(sudokuRender); //Drawing the sudoku grid and the game background
                    generateGrid(grid, gridPermission, gridSave); //Generate the grid
                    printGrid(sudokuRender, font,grid, gridPermission);//Print the previous generated grid on the screen
                    drawNumberButton(sudokuRender, font);

                    //If the game is drawed correctly
                    if(gameWinDrawed==0 && grid !=NULL){
                        //Launch the game loop
                        int gameloop=1;
                        Uint32 timer;
                        SDL_Rect* hover;
                        SDL_Rect* selected;
                        SDL_Event event;
                        while(gameloop){
                            static int timeRef=0;
                            static int seconds=0;
                            static int minute=0;
                            static int hours=0;
                            while(SDL_PollEvent(&event)){
                                int x,y, indexX, indexY;
                                switch(event.type){
                                    case SDL_QUIT:
                                        gameloop=0;
                                        break;
                                    case SDL_MOUSEMOTION:
                                        x=event.motion.x-OFFSET;
                                        y=event.motion.y-OFFSET;

                                        indexX = x/SQUARE_SIZE;
                                        indexY= y/SQUARE_SIZE;

                                            if((x >= 0 && x <9*SQUARE_SIZE) && (y >= 0 && y<9*SQUARE_SIZE)){

                                                if(gridPermission[indexY][indexX]){
                                                    if(!hover){
                                                        hover = malloc(sizeof(SDL_Rect));
                                                    }
                                                    hover->x = indexX*SQUARE_SIZE + OFFSET;
                                                    hover->y = indexY*SQUARE_SIZE + OFFSET;
                                                    hover->w = SQUARE_SIZE - 2;
                                                    hover->h = SQUARE_SIZE - 2;
                                               }else{
                                                    free(hover);
                                                    hover=NULL;
                                                }

                                                SDL_Log("(%d,%d) & (%d,%d)", indexX, indexY,x,y);
                                            }else if((x >=0 && x<9*SQUARE_SIZE) && (y>=718 && y<718+SQUARE_SIZE)){
                                                    if(!hover){
                                                        hover = malloc(sizeof(SDL_Rect));
                                                    }
                                                    hover->x = indexX*SQUARE_SIZE + OFFSET;
                                                    hover->y = 818;
                                                    hover->w = SQUARE_SIZE - 2;
                                                    hover->h = SQUARE_SIZE - 2;
                                            }else{
                                                free(hover);
                                                hover=NULL;
                                            }
                                            break;

                                    case SDL_MOUSEBUTTONDOWN:
                                        x=event.motion.x-OFFSET;
                                        y=event.motion.y-OFFSET;

                                        indexX = x/SQUARE_SIZE;
                                        indexY= y/SQUARE_SIZE;

                                        if((x >= 0 && x <9*SQUARE_SIZE) && (y >= 0 && y<9*SQUARE_SIZE)){

                                                if(gridPermission[indexY][indexX]){
                                                    selected = malloc(sizeof(SDL_Rect));
                                                    selected->x=indexX*SQUARE_SIZE + OFFSET;
                                                    selected->y=indexY*SQUARE_SIZE + OFFSET;
                                                    selected->w = SQUARE_SIZE-2;
                                                    selected->h = SQUARE_SIZE-2;
                                                }else{
                                                    free(selected);
                                                    selected=NULL;
                                                }
                                                SDL_Log("(%d,%d) & (%d,%d)", indexX, indexY,x,y);
                                        }else if((x >=0 && x<9*SQUARE_SIZE) && (y>=718 && y<718+SQUARE_SIZE)){
                                                if(selected){
                                                    if(isInLine(grid,(selected->y -OFFSET)/SQUARE_SIZE ,indexX+1)==0){
                                                        if(isInColumn(grid,(selected->x -OFFSET)/SQUARE_SIZE, indexX+1)==0){
                                                            if(isInSquare(grid, (selected->x - OFFSET)/(3*SQUARE_SIZE), (selected->y - OFFSET)/(3*SQUARE_SIZE), indexX+1)==0){
                                                                grid[(selected->y -OFFSET)/SQUARE_SIZE][(selected->x -OFFSET)/SQUARE_SIZE]=indexX+1;
                                                                free(selected);
                                                                selected=NULL;
                                                            }else{
                                                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Sudoku", "Ce chiffre et déja dans la zone !", NULL);
                                                                free(selected);
                                                                selected=NULL;
                                                            }
                                                        }else{
                                                            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Sudoku", "Ce chiffre et déja dans la colonne !", NULL);
                                                            free(selected);
                                                            selected=NULL;
                                                        }
                                                    }else{
                                                        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Sudoku", "Ce chiffre et déja dans la ligne !", NULL);
                                                        free(selected);
                                                        selected=NULL;
                                                    }

                                                }
                                        }else{
                                            free(selected);
                                            selected=NULL;
                                        }
                                        break;
                                    case SDL_KEYDOWN:

                                        if(event.key.keysym.sym == SDLK_DELETE){
                                            if(selected){
                                                if(grid[(selected->y -OFFSET)/SQUARE_SIZE][(selected->x -OFFSET)/SQUARE_SIZE] != 0){
                                                    grid[(selected->y -OFFSET)/SQUARE_SIZE][(selected->x -OFFSET)/SQUARE_SIZE]=0;
                                                    free(selected);
                                                    selected=NULL;
                                                }
                                            }
                                        }else if(event.key.keysym.sym == SDLK_ESCAPE){
                                            free(selected);
                                            selected=NULL;
                                        }
                                        break;
                                }
                            }
                            //If the grid isn't complete
                            if(isFinished(grid, gridSave)==1){
                                //Updating screen
                                drawBG(sudokuRender);
                                printGrid(sudokuRender, font,grid, gridPermission);
                                drawNumberButton(sudokuRender, font);

                                if(hover){

                                    SDL_SetRenderDrawColor(sudokuRender,0,0,0, 100);//Set the color of the lines
                                    SDL_SetRenderDrawBlendMode(sudokuRender, SDL_BLENDMODE_BLEND);//Setting blend mode
                                    SDL_RenderDrawRect(sudokuRender, hover);
                                    SDL_RenderFillRect(sudokuRender, hover);
                                    SDL_SetRenderDrawBlendMode(sudokuRender, SDL_BLENDMODE_NONE);//Setting blend mode
                                }

                                if(selected){
                                    SDL_SetRenderDrawColor(sudokuRender,255,255,255, 100);//Set the color of the lines
                                    SDL_SetRenderDrawBlendMode(sudokuRender, SDL_BLENDMODE_BLEND);//Setting blend mode
                                    SDL_RenderDrawRect(sudokuRender, selected);
                                    SDL_RenderFillRect(sudokuRender, selected);
                                    SDL_SetRenderDrawBlendMode(sudokuRender, SDL_BLENDMODE_NONE);//Setting blend mode
                                }

                                drawGrid(sudokuRender);
                                timer=SDL_GetTicks();
                                if(timer - timeRef > 1000){
                                    timeRef=timeRef+1000;
                                    seconds++;
                                }

                                if(seconds == 60){
                                    seconds=0;
                                    minute++;
                                }

                                if(minute==59){
                                    minute=0;
                                    hours++;
                                }
                                writeTimer(sudokuRender, font, seconds, minute, hours);
                                SDL_RenderPresent(sudokuRender);
                                SDL_Delay(1000/30);
                            }else{
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,"Sudoku", "Vous avez gagné !", NULL);
                                gameloop=0;
                                break;
                            }
                        }
                    }
                }
                //Free the memory
                SDL_DestroyRenderer(sudokuRender);
            }else{
                SDL_Log("Couldn't create the renderer: %s\n", SDL_GetError());
                return -1;
            }
            SDL_DestroyWindow(sudokuGame);
        }else{
            SDL_Log("Couldn't create window: %s\n", SDL_GetError());
            return -1;
        }
        TTF_CloseFont(font);
        for(int i=0;i<9;i++){
            free(grid[i]);
            free(gridPermission[i]);
            free(gridSave[i]);
        }
        free(grid);
        free(gridPermission);
    }else{
        SDL_Log("Couldn't initialize the SDL: %s\n", SDL_GetError());
        return -1;

    }

    TTF_Quit();
    SDL_Quit();
    return 0;

}

