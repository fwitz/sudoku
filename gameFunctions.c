#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include <time.h>

#include "gameRenderingFunctions.h"

#define COUNT 1  // Number of sudokus to generate
#define SHUFF 10 // Number of times the souce
#define HINTS 50 // Approximate percentage of cells to not delete in the generated grid

//Source sudoku grid
static int source[81] ={
	1,3,4,5,2,8,6,9,7,
	2,5,6,7,9,1,3,4,8,
	7,8,9,3,4,6,1,2,5,
	3,1,2,4,6,7,5,8,9,
	5,4,7,9,8,3,2,1,6,
	9,6,8,1,5,2,7,3,4,
	4,7,5,2,1,9,8,6,3,
	8,9,1,6,3,5,4,7,2,
	6,2,3,8,7,4,9,5,1
};

//Function that swap two rows on the grid
void swapRow(int from, int to)
{
	int *ptr1, *ptr2, i, temp;
	ptr1 = source+(9*from);
	ptr2 = source+(9*to);
	for(i=0; i<9; i++){
		temp = *ptr1;
		*ptr1 = *ptr2;
		*ptr2 = temp;
		ptr1++;
		ptr2++;
	}
}

//Function that swap two columns on the grid
void swapCol(int from, int to)
{
	int *ptr1, *ptr2, i, temp;
	ptr1 = source+from;
	ptr2 = source+to;
	for(i=0; i<9; i++){
		temp = *ptr1;
		*ptr1 = *ptr2;
		*ptr2 = temp;
		ptr1+=9;
		ptr2+=9;
	}
}

int isInLine(int** grid, int lineIndex, int value){
    for(int i=0;i<9;i++){
        if(grid[lineIndex][i]==value){
            return 1;
        }
    }
    return 0;
}

int isInColumn(int** grid, int columnIndex, int value){
    for(int i=0;i<9;i++){
        if(grid[i][columnIndex]==value){
            return 1;
        }
    }
    return 0;
}
int isInSquare(int** grid, int squareX, int squareY, int value){


    for(int i=squareX*3;i<squareX*3 + 3;i++){
        for(int j=squareY*3;j<squareY*3 + 3;j++){
            if(grid[j][i]==value){
                return 1;
            }
        }
    }
    return 0;
}

//Function that generate the sudoku grid with empty space
void generateGrid(int** grid, int** gridPermission, int** gridSave){

    srand(time(NULL));

	int i, j, swap, trio;

	for(i=0; i<COUNT; i++){
		for(j=0; j<SHUFF; j++){

			trio = (rand() % 3)*3;
			swap = rand() & 1;

			switch(rand() % 6){
				/* swap rows */
				case 0:
					swapRow(trio+0, swap ? trio+1 : trio+2);
					break;
				case 1:
					swapRow(trio+1, swap ? trio+0 : trio+2);
					break;
				case 2:
					swapRow(trio+2, swap ? trio+0 : trio+1);
					break;

				/* swap cols */
				case 3:
					swapCol(trio+0, swap ? trio+1 : trio+2);
					break;
				case 4:
					swapCol(trio+1, swap ? trio+0 : trio+2);
					break;
				case 5:
					swapCol(trio+2, swap ? trio+0 : trio+1);
					break;
			}

		}

	}

    //Copying the shuffled source grid into the final grid and deleting randomly some numbers
	int* ptr=source;
	for(int i=0; i<9; i++){
        for(int k=0; k<9; k++){
            if(rand()%100 > HINTS){
                grid[i][k]=*ptr;
                gridPermission[i][k]=0;
            }else{
                grid[i][k]=0;
                gridPermission[i][k]=1;
            }
            gridSave[i][k]=*ptr;
            ptr++;
        }
    }
}

//Function that print the grid, to refresh the screen
void printGrid(SDL_Renderer* renderer, TTF_Font* font, int** grid, int** gridPermission){

    for(int i=0; i<9; i++){
            for(int k=0; k<9; k++){
                if(grid[i][k] != 0){
                    if(!gridPermission[i][k]){
                        writeNumber(renderer, font, grid[i][k],i,k,0);
                    }else{
                        writeNumber(renderer, font, grid[i][k],i,k,1);
                    }
                }
            }
        }
}

int isFinished(int** grid, int** gridSave){

    for(int i=0;i<9;i++){
        for(int j=0;j<9;j++){
            if(gridSave[i][j]==grid[i][j]){
                return 1;
            }
        }
    }
    return 0;
}
