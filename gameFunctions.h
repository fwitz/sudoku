#ifndef GAMEFUNCTIONS_H_INCLUDED
#define GAMEFUNCTIONS_H_INCLUDED

void generateGrid(int** ,int**, int**);
void swap_row(int , int );
void swap_col(int , int );
void printGrid(SDL_Renderer* , TTF_Font*, int**, int**);
int isInLine(int**, int, int);
int isInColumn(int**, int, int);
int isInSquare(int**, int, int, int);
int isFinished(int**, int**);

#endif // GAMEFUNCTIONS_H_INCLUDED
